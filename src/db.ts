import {Connection, createConnection} from "typeorm";
import {EducationOrmEntity} from "./modules/education-persistence/education.orm-entity";
import {PassportOrmEntity} from "./modules/passport-persistence/passport.orm-entity";
import {PersonOrmEntity} from "./modules/person-persistence/person.orm-entity";
import {CompanyOrmEntity} from "./modules/company-persistence/company.orm-entity";
import {EmployeeOrmEntity} from "./modules/employee-persistence/employee.orm-entity";

let conn: Connection;

export const createDbConnection = async database => {
    conn = await createConnection({
        type: "sqlite",
        database: database,
        entities: [
            EducationOrmEntity,
            PassportOrmEntity,
            PersonOrmEntity,
            EmployeeOrmEntity,
            CompanyOrmEntity,
        ],
        synchronize: true,
    });
};

export const getConnection = () => conn;
