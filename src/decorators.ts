import {container} from "./di-container";
import {inject, injectable, interfaces} from "inversify";
import {ipcMain} from 'electron';

type ClassProvider = new (...args) => any;

type FactoryProvider = {
    provide: Symbol,
    useFactory: (...args) => any,
    inject: (new (...args) => any)[]
};

type ModuleArguments = {
    providers?: (ClassProvider | FactoryProvider)[];
};

export const Module = (args: ModuleArguments) => {
    if (args.providers) {
        for (const provider of args.providers) {
            if (typeof (<FactoryProvider>provider).useFactory === 'function') {
                container
                    .bind(<interfaces.ServiceIdentifier<any>>(<FactoryProvider>provider).provide)
                    .toConstantValue((<FactoryProvider>provider).useFactory.apply(null, (<FactoryProvider>provider).inject.map(i => container.get(i))));
                container.get(<interfaces.ServiceIdentifier<any>>(<FactoryProvider>provider).provide);
            } else {
                container.bind(<interfaces.ServiceIdentifier<any>>provider).to(<ClassProvider>provider).inSingletonScope();
                container.get(<interfaces.ServiceIdentifier<any>>provider);
            }
        }
    }

    return target => {
        const t = injectable()(target);
    };
};

export const Injectable = injectable;
export const Inject = inject;

const ROUTE_METADATA_KEY = 'controller:route';

export const Controller = (rootPath: string) => {
    if (rootPath && !rootPath.startsWith('/'))
        rootPath = '/' + rootPath;

    return target => {
        const descriptors = Object.getOwnPropertyDescriptors(target.prototype);
        for (const key in descriptors) {
            const value = descriptors[key].value;
            if (typeof value === 'function') {
                if (Reflect.hasMetadata(ROUTE_METADATA_KEY, value)) {
                    const route = Reflect.getMetadata(ROUTE_METADATA_KEY, value);
                    const path = rootPath + route
                    ipcMain.on(path, async (event, id, ...args) => {
                        let result, error;
                        try {
                            result = value.apply(container.get(target), args);
                            if (result instanceof Promise)
                                result = await result;

                            event.reply(path, id, null, result);
                        } catch (err) {
                            console.error(err);
                            error = err.message;
                            event.reply(path, id, error);
                        }
                    });
                }
            }
        }

        injectable()(target);
    };
};

export const Route = (route: string) => {
    if (route && !route.startsWith('/'))
        route = '/' + route;

    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        Reflect.defineMetadata(ROUTE_METADATA_KEY, route, descriptor.value);
    };
};
