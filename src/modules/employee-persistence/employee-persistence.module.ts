import {Module} from "../../decorators";
import {EmployeeAdapter} from "./employee.adapter";
import {
    ChangeEmployeeService,
    ChangeEmployeeUseCaseSymbol,
    CreateEmployeeService,
    CreateEmployeeUseCaseSymbol,
    FindEmployeeQuerySymbol,
    FindEmployeeService, RemoveEmployeeService, RemoveEmployeeUseCaseSymbol
} from "human-resources-department-common";

@Module({
    providers: [
        EmployeeAdapter,
        {
            provide: FindEmployeeQuerySymbol,
            useFactory: employeeAdapter => new FindEmployeeService(employeeAdapter),
            inject: [EmployeeAdapter]
        },
        {
            provide: CreateEmployeeUseCaseSymbol,
            useFactory: employeeAdapter => new CreateEmployeeService(employeeAdapter, employeeAdapter),
            inject: [EmployeeAdapter]
        },
        {
            provide: RemoveEmployeeUseCaseSymbol,
            useFactory: employeeAdapter => new RemoveEmployeeService(employeeAdapter, employeeAdapter),
            inject: [EmployeeAdapter]
        },
        {
            provide: ChangeEmployeeUseCaseSymbol,
            useFactory: employeeAdapter => new ChangeEmployeeService(employeeAdapter, employeeAdapter),
            inject: [EmployeeAdapter]
        },
    ]
})
export class EmployeePersistenceModule {}
