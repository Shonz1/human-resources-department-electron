import {Injectable} from "../../decorators";
import {getConnection} from "../../db";
import {EmployeeOrmEntity} from "./employee.orm-entity";
import {
    CompanyId,
    EmployeeEntity,
    EmployeeId,
    LoadEmployeePort,
    PersonId,
    UpdateEmployeePort
} from "human-resources-department-common";
import {EmployeeMapper} from "./employee.mapper";
import {PersonOrmEntity} from "../person-persistence/person.orm-entity";
import {randomUuid} from "../../random";

@Injectable()
export class EmployeeAdapter implements LoadEmployeePort, UpdateEmployeePort {
    private readonly _employeeRepository = getConnection().getRepository(EmployeeOrmEntity);
    private readonly _personRepository = getConnection().getRepository(PersonOrmEntity);

    async findEmployeeById(employeeId: EmployeeId): Promise<EmployeeEntity | null> {
        return EmployeeMapper.mapToDomain(await this._employeeRepository.findOne({where: {id: employeeId}}));
    }

    async findEmployeeByPersonIdAndCompanyId(personId: PersonId, companyId: CompanyId): Promise<EmployeeEntity | null> {
        return EmployeeMapper.mapToDomain(await this._employeeRepository.findOne({where: {personId, companyId}}));
    }

    async findEmployeesByPersonId(personId: PersonId): Promise<EmployeeEntity[]> {
        return (await this._employeeRepository.find({where: {personId}})).map(i => EmployeeMapper.mapToDomain(i));
    }

    async findEmployeesByCompanyId(companyId: CompanyId): Promise<EmployeeEntity[]> {
        return (await this._employeeRepository.find({where: {companyId}})).map(i => EmployeeMapper.mapToDomain(i));
    }

    async findEmployeesOlderThan(age: number): Promise<EmployeeEntity[]> {
        const now = Date.now();
        const ageTimestamp = 1000 * 60 * 60 * 24 * 256 * age;

        return (await Promise.all(
            (await this._employeeRepository.find({})).map(async employee => {
                const person = await this._personRepository.findOne({where: {id: employee.personId}});
                if (now - ageTimestamp >= person.birthday.valueOf())
                    return employee;
                return null;
            })
        )).filter(i => !!i).map(i => EmployeeMapper.mapToDomain(i));
    }

    async findAllEmployees(): Promise<EmployeeEntity[]> {
        return (await this._employeeRepository.find({})).map(i => EmployeeMapper.mapToDomain(i));
    }

    async createEmployee(employee: EmployeeEntity): Promise<EmployeeEntity> {
        const insertData = new EmployeeEntity(
            randomUuid(),
            employee.personId,
            employee.companyId,
            employee.role,
            employee.salary,
            employee.socialPrivilege,
            employee.employmentAt,
            employee.firedAt,
        );

        await this._employeeRepository.insert(EmployeeMapper.mapToOrm(insertData));

        return insertData;
    }

    async removeEmployee(employee: EmployeeEntity): Promise<void> {
        await this._employeeRepository.delete(employee.id);
    }

    async changeEmployeeRole(employee: EmployeeEntity): Promise<void> {
        await this._employeeRepository.update(employee.id, {role: employee.role});
    }

    async changeEmployeeSalary(employee: EmployeeEntity): Promise<void> {
        await this._employeeRepository.update(employee.id, {salary: employee.salary});
    }

    async changeEmployeeSocialPrivilege(employee: EmployeeEntity): Promise<void> {
        await this._employeeRepository.update(employee.id, {socialPrivilege: employee.socialPrivilege});
    }

    async changeEmployeeFiredAt(employee: EmployeeEntity): Promise<void> {
        console.log(employee);
        await this._employeeRepository.update(employee.id, {firedAt: employee.firedAt});
    }
}
