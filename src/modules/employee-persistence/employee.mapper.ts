import {EmployeeEntity} from "human-resources-department-common";
import {EmployeeOrmEntity} from "./employee.orm-entity";

export class EmployeeMapper {
    static mapToDomain(employee): EmployeeEntity {
        if (employee instanceof EmployeeOrmEntity) {
            return new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                employee.role,
                employee.salary,
                employee.socialPrivilege,
                employee.employmentAt,
                employee.firedAt,
            );
        }

        return null;
    }

    static mapToOrm(employee): EmployeeOrmEntity {
        if (!employee)
            return null;

        const ormEntity = new EmployeeOrmEntity();

        if (employee instanceof EmployeeEntity) {
            ormEntity.id = employee.id;
            ormEntity.personId = employee.personId;
            ormEntity.companyId = employee.companyId;
            ormEntity.role = employee.role;
            ormEntity.salary = employee.salary;
            ormEntity.socialPrivilege = employee.socialPrivilege;
            ormEntity.employmentAt = employee.employmentAt;
            ormEntity.firedAt = employee.firedAt;
        }

        return ormEntity;
    }
}
