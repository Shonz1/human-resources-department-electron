import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('employees')
export class EmployeeOrmEntity {
    @PrimaryColumn({name: 'id'})
    id: string;

    @Column({name: 'person_id'})
    personId: string;

    @Column({name: 'company_id'})
    companyId: string;

    @Column({name: 'role'})
    role: string;

    @Column({name: 'salary'})
    salary: number;

    @Column({name: 'social_privilege'})
    socialPrivilege: boolean;

    @Column({name: 'employment_at'})
    employmentAt: Date;

    @Column({name: 'fired_at', nullable: true})
    firedAt: Date;
}
