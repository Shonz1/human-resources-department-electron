export class CompanyObjectEntity {
    id: string;
    shortName: string;
    fullName: string;
    internationalName: string;
    address: string;
    phone: string;
    director?: string;
}
