import {Module} from "../../decorators";
import {CompanyService} from "./company.service";
import {CompanyController} from "./company.controller";

@Module({
    providers: [
        CompanyService,
        CompanyController,
    ]
})
export class CompanyModule {}

