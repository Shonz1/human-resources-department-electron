import {CompanyObjectEntity} from "./company.object-entity";
import {CompanyEntity} from "human-resources-department-common";

export class CompanyMapper {
    static mapToDomain(company): CompanyEntity {
        if (company instanceof CompanyObjectEntity) {
            return new CompanyEntity(
                company.id,
                company.shortName,
                company.fullName,
                company.internationalName,
                company.address,
                company.phone,
                company.director,
            );
        }

        return null;
    }

    static mapToObject(company): CompanyObjectEntity {
        if (!company)
            return null;

        const obj = new CompanyObjectEntity();

        if (company instanceof CompanyEntity) {
            obj.id = company.id;
            obj.shortName = company.shortName;
            obj.fullName = company.fullName;
            obj.internationalName = company.internationalName;
            obj.address = company.address;
            obj.phone = company.phone;
            obj.director = company.director;
        }

        return obj;
    }
}
