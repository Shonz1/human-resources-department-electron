import {Controller, Inject, Route} from "../../decorators";
import {CompanyService} from "./company.service";

@Controller('company')
export class CompanyController {
    constructor(
        @Inject(CompanyService) private readonly _companyService: CompanyService,
    ) {}

    @Route('findCompanyById')
    async findCompanyById(id: string) {
        return await this._companyService.findCompanyById(id);
    }

    @Route('findCompanyByName')
    async findCompanyByName(name: string) {
        return await this._companyService.findCompanyByName(name);
    }

    @Route('findAllCompanies')
    async findAllCompanies() {
        return await this._companyService.findAllCompanies();
    }

    @Route('createCompany')
    async createCompany(id: string, shortName: string, fullName: string, internationalName: string, address: string, phone: string) {
        return await this._companyService.createCompany(id, shortName, fullName, internationalName, address, phone);
    }

    @Route('removeCompany')
    async removeCompany(id: string) {
        return this._companyService.removeCompany(id);
    }

    @Route('changeCompanyDirector')
    async changeCompanyDirector(companyId: string, directorId: string) {
        return this._companyService.changeCompanyDirector(companyId, directorId);
    }
}
