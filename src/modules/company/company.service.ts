import {
    ChangeCompanyDirectorCommand,
    ChangeCompanyUseCase,
    ChangeCompanyUseCaseSymbol,
    CreateCompanyCommand,
    CreateCompanyUseCase,
    CreateCompanyUseCaseSymbol,
    FindCompanyQuery,
    FindCompanyQuerySymbol, RemoveCompanyCommand, RemoveCompanyUseCase, RemoveCompanyUseCaseSymbol
} from "human-resources-department-common";
import {Inject, Injectable} from "../../decorators";
import {CompanyMapper} from "./company.mapper";

@Injectable()
export class CompanyService {
    constructor(
        @Inject(FindCompanyQuerySymbol) private readonly _findCompanyQuery: FindCompanyQuery,
        @Inject(CreateCompanyUseCaseSymbol) private readonly _createCompanyUseCase: CreateCompanyUseCase,
        @Inject(RemoveCompanyUseCaseSymbol) private readonly _removeCompanyUseCase: RemoveCompanyUseCase,
        @Inject(ChangeCompanyUseCaseSymbol) private readonly _changeCompanyUseCase: ChangeCompanyUseCase,
    ) {}

    async findCompanyById(id: string) {
        return CompanyMapper.mapToObject(await this._findCompanyQuery.findCompanyById(id));
    }

    async findCompanyByName(name: string) {
        return CompanyMapper.mapToObject(await this._findCompanyQuery.findCompanyByName(name));
    }

    async findAllCompanies() {
        return (await this._findCompanyQuery.findAllCompanies()).map(i => CompanyMapper.mapToObject(i));
    }

    async createCompany(id: string, shortName: string, fullName: string, internationalName: string, address: string, phone: string) {
        return CompanyMapper.mapToObject(await this._createCompanyUseCase.createCompany(
            new CreateCompanyCommand(
                id,
                shortName,
                fullName,
                internationalName,
                address,
                phone,
            )
        ));
    }

    async removeCompany(id: string) {
        return await this._removeCompanyUseCase.removeCompany(
            new RemoveCompanyCommand(id)
        )
    }

    async changeCompanyDirector(companyId: string, directorId: string): Promise<void> {
        return await this._changeCompanyUseCase.changeCompanyDirector(
            new ChangeCompanyDirectorCommand(companyId, directorId)
        );
    }
}
