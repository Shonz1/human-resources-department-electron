import {
    LoadPassportPort,
    PassportEntity,
    PassportId,
    PersonId,
    UpdatePassportPort
} from "human-resources-department-common";
import {Injectable} from "../../decorators";
import {getConnection} from "../../db";
import {PassportOrmEntity} from "./passport.orm-entity";
import {PassportMapper} from "./passport.mapper";

@Injectable()
export class PassportAdapter implements LoadPassportPort, UpdatePassportPort {
    private readonly _passportRepository = getConnection().getRepository(PassportOrmEntity);

    async findPassportById(passportId: PassportId): Promise<PassportEntity | null> {
        return PassportMapper.mapToDomain(await this._passportRepository.findOne({where: {id: passportId}}));
    }

    async findPassportBySerialAndNumber(serial: string, number: string): Promise<PassportEntity | null> {
        return PassportMapper.mapToDomain(await this._passportRepository.findOne({where: {serial, number}}));
    }

    async findPassportsByPersonId(personId: PersonId): Promise<PassportEntity[]> {
        return (await this._passportRepository.find({where: {personId}})).map(i => PassportMapper.mapToDomain(i));
    }

    async findAllPassports(): Promise<PassportEntity[]> {
        return (await this._passportRepository.find({})).map(i => PassportMapper.mapToDomain(i));
    }

    async createPassport(passport: PassportEntity): Promise<PassportEntity> {
        await this._passportRepository.insert(PassportMapper.mapToOrm(passport));
        return passport;
    }

    async removePassport(passport: PassportEntity): Promise<void> {
        await this._passportRepository.delete(passport.id);
    }
}
