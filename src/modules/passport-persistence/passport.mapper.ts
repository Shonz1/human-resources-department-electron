import {PassportEntity} from "human-resources-department-common";
import {PassportOrmEntity} from "./passport.orm-entity";

export class PassportMapper {
    static mapToDomain(passport): PassportEntity {
        if (passport instanceof PassportOrmEntity) {
            return new PassportEntity(
                passport.id,
                passport.personId,
                passport.serial,
                passport.number,
                passport.issuer,
                passport.issuedAt,
            );
        }

        return null;
    }

    static mapToOrm(passport): PassportOrmEntity {
        if (!passport)
            return null;

        const ormEntity = new PassportOrmEntity();

        if (passport instanceof PassportEntity) {
            ormEntity.id = passport.id;
            ormEntity.personId = passport.personId;
            ormEntity.serial = passport.serial;
            ormEntity.number = passport.number;
            ormEntity.issuer = passport.issuer;
            ormEntity.issuedAt = passport.issuedAt;
        }

        return ormEntity;
    }
}
