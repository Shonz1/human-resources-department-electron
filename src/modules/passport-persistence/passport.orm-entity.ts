import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('passports')
export class PassportOrmEntity {
    @PrimaryColumn({name: 'id'})
    id: string;

    @Column({name: 'person_id'})
    personId: string;

    @Column({name: 'serial'})
    serial: string;

    @Column({name: 'number'})
    number: string;

    @Column({name: 'issuer'})
    issuer: string;

    @Column({name: 'issued_at'})
    issuedAt: Date;
}
