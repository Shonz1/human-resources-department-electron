import {Module} from "../../decorators";
import {PassportAdapter} from "./passport.adapter";
import {
    CreatePassportService,
    CreatePassportUseCaseSymbol,
    FindPassportQuerySymbol,
    FindPassportService, RemovePassportService, RemovePassportUseCaseSymbol
} from "human-resources-department-common";

@Module({
    providers: [
        PassportAdapter,
        {
            provide: FindPassportQuerySymbol,
            useFactory: passportAdapter => new FindPassportService(passportAdapter),
            inject: [PassportAdapter]
        },
        {
            provide: CreatePassportUseCaseSymbol,
            useFactory: passportAdapter => new CreatePassportService(passportAdapter, passportAdapter),
            inject: [PassportAdapter]
        },
        {
            provide: RemovePassportUseCaseSymbol,
            useFactory: passportAdapter => new RemovePassportService(passportAdapter, passportAdapter),
            inject: [PassportAdapter]
        }
    ]
})
export class PassportPersistenceModule {}
