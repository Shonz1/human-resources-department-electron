import {Module} from "../../decorators";
import {EmployeeService} from "./employee.service";
import {EmployeeController} from "./employee.controller";

@Module({
    providers: [
        EmployeeService,
        EmployeeController,
    ]
})
export class EmployeeModule {}
