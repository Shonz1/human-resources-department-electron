export class EmployeeObjectEntity {
    id: string;
    personId: string;
    companyId: string;
    role: string;
    salary: number;
    socialPrivilege: boolean;
    employmentAt: Date;
    firedAt: Date;
}
