import {Inject, Injectable} from "../../decorators";
import {
    ChangeEmployeeFiredAtCommand,
    ChangeEmployeeRoleCommand, ChangeEmployeeSalaryCommand, ChangeEmployeeSocialPrivilegeCommand,
    ChangeEmployeeUseCase,
    ChangeEmployeeUseCaseSymbol,
    CreateEmployeeCommand,
    CreateEmployeeUseCase,
    CreateEmployeeUseCaseSymbol,
    FindEmployeeQuery,
    FindEmployeeQuerySymbol, RemoveEmployeeCommand, RemoveEmployeeUseCase, RemoveEmployeeUseCaseSymbol
} from "human-resources-department-common";
import {EmployeeObjectEntity} from "./employee.object-entity";
import { EmployeeMapper } from "./employee.mapper";

@Injectable()
export class EmployeeService {
    constructor(
        @Inject(FindEmployeeQuerySymbol) private readonly _findEmployeeQuery: FindEmployeeQuery,
        @Inject(CreateEmployeeUseCaseSymbol) private readonly _createEmployeeUseCase: CreateEmployeeUseCase,
        @Inject(RemoveEmployeeUseCaseSymbol) private readonly _removeEmployeeUseCase: RemoveEmployeeUseCase,
        @Inject(ChangeEmployeeUseCaseSymbol) private readonly _changeEmployeeUseCase: ChangeEmployeeUseCase,
    ) {}

    async findEmployeeById(employeeId: string): Promise<EmployeeObjectEntity | null> {
        return EmployeeMapper.mapToObject(await this._findEmployeeQuery.findEmployeeById(employeeId));
    }

    async findEmployeeByPersonIdAndCompanyId(personId: string, companyId: string): Promise<EmployeeObjectEntity | null> {
        return EmployeeMapper.mapToObject(await this._findEmployeeQuery.findEmployeeByPersonIdAndCompanyId(personId, companyId));
    }

    async findEmployeesByPersonId(personId: string): Promise<EmployeeObjectEntity[]> {
        return (await this._findEmployeeQuery.findEmployeesByPersonId(personId)).map(i => EmployeeMapper.mapToObject(i));
    }

    async findEmployeesByCompanyId(companyId: string): Promise<EmployeeObjectEntity[]> {
        return (await this._findEmployeeQuery.findEmployeesByCompanyId(companyId)).map(i => EmployeeMapper.mapToObject(i));
    }

    async findEmployeesOlderThan(age: number): Promise<EmployeeObjectEntity[]> {
        return (await this._findEmployeeQuery.findEmployeesOlderThan(age)).map(i => EmployeeMapper.mapToObject(i));
    }

    async findAllEmployees(): Promise<EmployeeObjectEntity[]> {
        return (await this._findEmployeeQuery.findAllEmployees()).map(i => EmployeeMapper.mapToObject(i));
    }

    async createEmployee(personId: string, companyId: string, role: string, salary: number, socialPrivilege: boolean): Promise<EmployeeObjectEntity> {
        return EmployeeMapper.mapToObject(await this._createEmployeeUseCase.createEmployee(
            new CreateEmployeeCommand(
                personId,
                companyId,
                role,
                salary,
                socialPrivilege,
            )
        ));
    }

    async removeEmployee(employeeId: string): Promise<void> {
        return await this._removeEmployeeUseCase.removeEmployee(
            new RemoveEmployeeCommand(employeeId)
        );
    }

    async changeEmployeeRole(employeeId: string, role: string): Promise<void> {
        return await this._changeEmployeeUseCase.changeEmployeeRole(
            new ChangeEmployeeRoleCommand(employeeId, role)
        );
    }

    async changeEmployeeSalary(employeeId: string, salary: number): Promise<void> {
        return await this._changeEmployeeUseCase.changeEmployeeSalary(
            new ChangeEmployeeSalaryCommand(employeeId, salary)
        );
    }

    async changeEmployeeSocialPrivilege(employeeId: string, socialPrivilege: boolean): Promise<void> {
        return await this._changeEmployeeUseCase.changeEmployeeSocialPrivilege(
            new ChangeEmployeeSocialPrivilegeCommand(employeeId, socialPrivilege)
        );
    }

    async changeEmployeeFiredAt(employeeId: string, firedAt: Date): Promise<void> {
        return await this._changeEmployeeUseCase.changeEmployeeFiredAt(
            new ChangeEmployeeFiredAtCommand(employeeId, firedAt)
        );
    }
}
