import {EmployeeEntity} from "human-resources-department-common";
import {EmployeeObjectEntity} from "./employee.object-entity";

export class EmployeeMapper {
    static mapToDomain(employee): EmployeeEntity {
        if (employee instanceof EmployeeObjectEntity) {
            return new EmployeeEntity(
                employee.id,
                employee.personId,
                employee.companyId,
                employee.role,
                employee.salary,
                employee.socialPrivilege,
                employee.employmentAt,
                employee.firedAt,
            );
        }

        return null;
    }

    static mapToObject(employee): EmployeeObjectEntity {
        if (!employee)
            return null;

        const obj = new EmployeeObjectEntity();

        if (employee instanceof EmployeeEntity) {
            obj.id = employee.id;
            obj.personId = employee.personId;
            obj.companyId = employee.companyId;
            obj.role = employee.role;
            obj.salary = employee.salary;
            obj.socialPrivilege = employee.socialPrivilege;
            obj.employmentAt = employee.employmentAt;
            obj.firedAt = employee.firedAt;
        }

        return obj;
    }
}
