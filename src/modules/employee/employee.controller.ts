import {Controller, Inject, Route} from "../../decorators";
import {EmployeeService} from "./employee.service";
import {ChangeEmployeeRoleCommand} from "human-resources-department-common";

@Controller('employee')
export class EmployeeController {
    constructor(
        @Inject(EmployeeService) private readonly _employeeService: EmployeeService,
    ) {}

    @Route('findEmployeeById')
    async findEmployeeById(employeeId: string) {
        return await this._employeeService.findEmployeeById(employeeId);
    }

    @Route('findEmployeeByPersonIdAndCompanyId')
    async findEmployeeByPersonIdAndCompanyId(personId: string, companyId: string) {
        return await this._employeeService.findEmployeeByPersonIdAndCompanyId(personId, companyId);
    }

    @Route('findEmployeesByPersonId')
    async findEmployeesByPersonId(personId: string) {
        return await this._employeeService.findEmployeesByPersonId(personId);
    }

    @Route('findEmployeesByCompanyId')
    async findEmployeesByCompanyId(companyId: string) {
        return await this._employeeService.findEmployeesByCompanyId(companyId);
    }

    @Route('findEmployeesOlderThan')
    async findEmployeesOlderThan(age: number) {
        return await this._employeeService.findEmployeesOlderThan(age);
    }

    @Route('findAllEmployees')
    async findAllEmployees() {
        return await this._employeeService.findAllEmployees();
    }

    @Route('createEmployee')
    async createEmployee(personId: string, companyId: string, role: string, salary: number, socialPrivilege: boolean) {
        return await this._employeeService.createEmployee(personId, companyId, role, salary, socialPrivilege);
    }

    @Route('removeEmployee')
    async removeEmployee(employeeId: string) {
        return await this._employeeService.removeEmployee(employeeId);
    }

    @Route('changeEmployeeRole')
    async changeEmployeeRole(employeeId: string, role: string): Promise<void> {
        return await this._employeeService.changeEmployeeRole(employeeId, role);
    }

    @Route('changeEmployeeSalary')
    async changeEmployeeSalary(employeeId: string, salary: number): Promise<void> {
        return await this._employeeService.changeEmployeeSalary(employeeId, salary);
    }

    @Route('changeEmployeeSocialPrivilege')
    async changeEmployeeSocialPrivilege(employeeId: string, socialPrivilege: boolean): Promise<void> {
        return await this._employeeService.changeEmployeeSocialPrivilege(employeeId, socialPrivilege);
    }

    @Route('changeEmployeeFiredAt')
    async changeEmployeeFiredAt(employeeId: string, firedAt: Date): Promise<void> {
        return await this._employeeService.changeEmployeeFiredAt(employeeId, firedAt);
    }
}
