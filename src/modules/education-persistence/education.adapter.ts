import {getConnection} from "../../db";
import {EducationOrmEntity} from "./education.orm-entity";
import {
    EducationEntity,
    EducationId,
    LoadEducationPort,
    PersonId,
    UpdateEducationPort
} from "human-resources-department-common";
import {EducationMapper} from "./education.mapper";
import {Injectable} from "../../decorators";
import {randomUuid} from "../../random";

@Injectable()
export class EducationAdapter implements LoadEducationPort, UpdateEducationPort {
    private readonly _educationRepository = getConnection().getRepository(EducationOrmEntity);

    async findEducationById(educationId: EducationId): Promise<EducationEntity | null> {
        return EducationMapper.mapToDomain(await this._educationRepository.findOne({where: {id: educationId}}));
    }

    async findEducationsByPersonId(personId: PersonId): Promise<EducationEntity[]> {
        return (await this._educationRepository.find({where: {personId}})).map(i => EducationMapper.mapToDomain(i));
    }

    async createEducation(education: EducationEntity): Promise<EducationEntity> {
        const insertData = new EducationEntity(
            randomUuid(),
            education.personId,
            education.name,
            education.level,
        );

        await this._educationRepository.insert(EducationMapper.mapToOrm(insertData));

        return insertData;
    }

    async removeEducation(education: EducationEntity): Promise<void> {
        await this._educationRepository.delete(education.id);
    }
}
