import {Module} from "../../decorators";
import {EducationAdapter} from "./education.adapter";
import {
    CreateEducationService,
    CreateEducationUseCaseSymbol,
    FindEducationQuerySymbol,
    FindEducationService, RemoveEducationService, RemoveEducationUseCaseSymbol
} from "human-resources-department-common";

@Module({
    providers: [
        EducationAdapter,
        {
            provide: FindEducationQuerySymbol,
            useFactory: educationAdapter => new FindEducationService(educationAdapter),
            inject: [EducationAdapter]
        },
        {
            provide: CreateEducationUseCaseSymbol,
            useFactory: educationAdapter => new CreateEducationService(educationAdapter),
            inject: [EducationAdapter]
        },
        {
            provide: RemoveEducationUseCaseSymbol,
            useFactory: educationAdapter => new RemoveEducationService(educationAdapter, educationAdapter),
            inject: [EducationAdapter]
        },
    ]

})
export class EducationPersistenceModule {}
