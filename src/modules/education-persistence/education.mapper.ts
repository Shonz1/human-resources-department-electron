import {EducationOrmEntity} from "./education.orm-entity";
import {EducationEntity} from "human-resources-department-common";

export class EducationMapper {
    static mapToDomain(education): EducationEntity {
        if (education instanceof EducationOrmEntity) {
            return new EducationEntity(
                education.id,
                education.personId,
                education.name,
                education.level,
            );
        }

        return null;
    }

    static mapToOrm(education): EducationOrmEntity {
        if (!education)
            return null;

        const ormEntity = new EducationOrmEntity();

        ormEntity.id = education.id
        ormEntity.personId = education.personId
        ormEntity.name = education.name
        ormEntity.level = education.level

        return ormEntity;
    }
}
