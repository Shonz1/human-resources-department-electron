import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('educations')
export class EducationOrmEntity {
    @PrimaryColumn({name: 'id'})
    id: string;

    @Column({name: 'person_id'})
    personId: string;

    @Column({name: 'name'})
    name: string;

    @Column({name: 'level'})
    level: number;
}
