import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('people')
export class PersonOrmEntity {
    @PrimaryColumn({name: 'id'})
    id: string;

    @Column({name: 'first_name'})
    firstName: string;

    @Column({name: 'last_name'})
    lastName: string;

    @Column({name: 'patronymic'})
    patronymic: string;

    @Column({name: 'gender'})
    gender: boolean;

    @Column({name: 'birthday'})
    birthday: Date;
}
