import {PersonOrmEntity} from "./person.orm-entity";
import {PersonEntity} from "human-resources-department-common";

export class PersonMapper {
    static mapToDomain(person): PersonEntity {
        if (person instanceof PersonOrmEntity) {
            return new PersonEntity(
                person.id,
                person.lastName,
                person.firstName,
                person.patronymic,
                person.gender,
                person.birthday,
            );
        }

        return null;
    }

    static mapToOrm(person): PersonOrmEntity {
        if (!person)
            return null;

        const ormEntity = new PersonOrmEntity();

        if (person instanceof PersonEntity) {
            ormEntity.id = person.id;
            ormEntity.lastName = person.lastName;
            ormEntity.firstName = person.firstName;
            ormEntity.patronymic = person.patronymic;
            ormEntity.gender = person.gender;
            ormEntity.birthday = person.birthday;
        }

        return ormEntity;
    }
}
