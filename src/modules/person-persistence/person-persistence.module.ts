import {Module} from "../../decorators";
import {PersonAdapter} from "./person.adapter";
import {
    CreatePersonService,
    CreatePersonUseCaseSymbol,
    FindPersonQuerySymbol,
    FindPersonService, RemovePersonService, RemovePersonUseCaseSymbol
} from "human-resources-department-common";

@Module({
    providers: [
        PersonAdapter,
        {
            provide: FindPersonQuerySymbol,
            useFactory: personAdapter => new FindPersonService(personAdapter),
            inject: [PersonAdapter]
        },
        {
            provide: CreatePersonUseCaseSymbol,
            useFactory: personAdapter => new CreatePersonService(personAdapter, personAdapter),
            inject: [PersonAdapter]
        },
        {
            provide: RemovePersonUseCaseSymbol,
            useFactory: personAdapter => new RemovePersonService(personAdapter, personAdapter),
            inject: [PersonAdapter]
        },
    ],
})
export class PersonPersistenceModule {}
