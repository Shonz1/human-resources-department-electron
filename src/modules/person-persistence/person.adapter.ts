import {LoadPersonPort, PersonEntity, PersonId, UpdatePersonPort} from "human-resources-department-common";
import {getConnection} from "../../db";
import {PersonOrmEntity} from "./person.orm-entity";
import {PersonMapper} from "./person.mapper";
import {Injectable} from "../../decorators";

@Injectable()
export class PersonAdapter implements LoadPersonPort, UpdatePersonPort {
    private readonly _personRepository = getConnection().getRepository(PersonOrmEntity);

    async findPersonById(personId: PersonId): Promise<PersonEntity | null> {
        return PersonMapper.mapToDomain(await this._personRepository.findOne({where: {id: personId}}));
    }

    async findPersonByFullName(lastName: string, firstName: string, patronymic: string): Promise<PersonEntity | null> {
        return PersonMapper.mapToDomain(await this._personRepository.findOne({where: {lastName, firstName, patronymic}}));
    }

    async findPeopleOlderThan(age: number): Promise<PersonEntity[]> {
        const now = Date.now();
        const ageTimestamp = 1000 * 60 * 60 * 24 * 256 * age;
        return (await this._personRepository.find({}))
            .filter(i => now - ageTimestamp >= i.birthday.valueOf())
            .map(i => PersonMapper.mapToDomain(i));
    }

    async findAllPeople(): Promise<PersonEntity[]> {
        return (await this._personRepository.find({})).map(i => PersonMapper.mapToDomain(i));
    }

    async createPerson(person: PersonEntity): Promise<PersonEntity> {
        await this._personRepository.insert(PersonMapper.mapToOrm(person));
        return person;
    }

    async removePerson(person: PersonEntity): Promise<void> {
        await this._personRepository.delete(person.id);
    }
}
