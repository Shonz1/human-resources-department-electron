import {Controller, Inject, Route} from "../../decorators";
import {PersonService} from "./person.service";

@Controller('person')
export class PersonController {
    constructor(
        @Inject(PersonService) private readonly _personService: PersonService,
    ) {}

    @Route('findPersonById')
    async findPersonById(personId: string) {
        return await this._personService.findPersonById(personId);
    }

    @Route('findPersonByFullName')
    async findPersonByFullName(lastName: string, firstName: string, patronymic: string) {
        return await this._personService.findPersonByFullName(lastName, firstName, patronymic);
    }

    @Route('findPeopleOlderThan')
    async findPeopleOlderThan(age: number) {
        return await this._personService.findPeopleOlderThan(age);
    }

    @Route('findAllPeople')
    async findAllPeople() {
        return await this._personService.findAllPeople();
    }

    @Route('createPerson')
    async createPerson(personId: string, lastName: string, firstName: string, patronymic: string, gender: boolean, birthday: Date) {
        return await this._personService.createPerson(personId, lastName, firstName, patronymic, gender, birthday);
    }

    @Route('removePerson')
    async removePerson(personId: string) {
        return await this._personService.removePerson(personId);
    }
}
