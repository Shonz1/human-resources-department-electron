import {Module} from "../../decorators";
import {PersonService} from "./person.service";
import {PersonController} from "./person.controller";

@Module({
    providers: [
        PersonService,
        PersonController,
    ],
})
export class PersonModule {}
