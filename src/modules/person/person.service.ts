import {Inject, Injectable} from "../../decorators";
import {
    CreatePersonCommand,
    CreatePersonUseCase,
    CreatePersonUseCaseSymbol,
    FindPersonQuery,
    FindPersonQuerySymbol, RemovePersonCommand, RemovePersonUseCase, RemovePersonUseCaseSymbol
} from "human-resources-department-common";
import {PersonMapper} from "./person.mapper";

@Injectable()
export class PersonService {
    constructor(
        @Inject(FindPersonQuerySymbol) private readonly _findPersonQuery: FindPersonQuery,
        @Inject(CreatePersonUseCaseSymbol) private readonly _createPersonUseCase: CreatePersonUseCase,
        @Inject(RemovePersonUseCaseSymbol) private readonly _removePersonUseCase: RemovePersonUseCase,
    ) {}

    async findPersonById(personId: string) {
        return PersonMapper.mapToObject(await this._findPersonQuery.findPersonById(personId));
    }

    async findPersonByFullName(lastName: string, firstName: string, patronymic: string) {
        return PersonMapper.mapToObject(await this._findPersonQuery.findPersonByFullName(lastName, firstName, patronymic));
    }

    async findPeopleOlderThan(age: number) {
        return (await this._findPersonQuery.findPeopleOlderThan(age)).map(i => PersonMapper.mapToObject(i));
    }

    async findAllPeople() {
        return (await this._findPersonQuery.findAllPeople()).map(i => PersonMapper.mapToObject(i));
    }

    async createPerson(personId: string, lastName: string, firstName: string, patronymic: string, gender: boolean, birthday: Date) {
        return PersonMapper.mapToObject(await this._createPersonUseCase.createPerson(
            new CreatePersonCommand(
                personId,
                lastName,
                firstName,
                patronymic,
                gender,
                birthday,
            )
        ));
    }

    async removePerson(personId: string) {
        return await this._removePersonUseCase.removePerson(
            new RemovePersonCommand(personId)
        );
    }
}
