export class PersonObjectEntity {
    id: string;
    firstName: string;
    lastName: string;
    patronymic: string;
    gender: boolean;
    birthday: Date;
}
