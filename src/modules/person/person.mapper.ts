import {PersonEntity} from "human-resources-department-common";
import {PersonObjectEntity} from "./person.object-entity";

export class PersonMapper {
    static mapToDomain(person): PersonEntity {
        if (person instanceof PersonObjectEntity) {
            return new PersonEntity(
                person.id,
                person.lastName,
                person.firstName,
                person.patronymic,
                person.gender,
                person.birthday,
            );
        }

        return null;
    }

    static mapToObject(person): PersonObjectEntity {
        if (!person)
            return null;

        const obj = new PersonObjectEntity();

        if (person instanceof PersonEntity) {
            obj.id = person.id;
            obj.lastName = person.lastName;
            obj.firstName = person.firstName;
            obj.patronymic = person.patronymic;
            obj.gender = person.gender;
            obj.birthday = person.birthday;
        }

        return obj;
    }
}
