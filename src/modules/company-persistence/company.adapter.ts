import {CompanyEntity, CompanyId, LoadCompanyPort, UpdateCompanyPort} from "human-resources-department-common";
import {Injectable} from "../../decorators";
import {CompanyMapper} from "./company.mapper";
import {getConnection} from "../../db";
import {CompanyOrmEntity} from "./company.orm-entity";

@Injectable()
export class CompanyAdapter implements LoadCompanyPort, UpdateCompanyPort {
    private readonly _companyRepository = getConnection().getRepository(CompanyOrmEntity);

    async findCompanyById(companyId: CompanyId): Promise<CompanyEntity | null> {
        return CompanyMapper.mapToDomain(await this._companyRepository.findOne({where: {id: companyId}}));
    }

    async findCompanyByName(name: string): Promise<CompanyEntity | null> {
        return CompanyMapper.mapToDomain(
            await this._companyRepository.findOne({
                where: [
                    {shortName: name},
                    {fullName: name},
                    {internationalName: name},
                ]
            })
        );
    }

    async findAllCompanies(): Promise<CompanyEntity[]> {
        return (await this._companyRepository.find({})).map(i => CompanyMapper.mapToDomain(i));
    }

    async createCompany(company: CompanyEntity): Promise<CompanyEntity> {
        await this._companyRepository.insert(CompanyMapper.mapToOrm(company));
        return company;
    }

    async removeCompany(company: CompanyEntity): Promise<void> {
        await this._companyRepository.delete(company.id);
    }

    async changeCompanyDirector(company: CompanyEntity): Promise<void> {
        await this._companyRepository.update(company.id, {director: company.director});
    }
}
