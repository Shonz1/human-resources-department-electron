import {CompanyEntity} from "human-resources-department-common";
import {CompanyOrmEntity} from "./company.orm-entity";

export class CompanyMapper {
    static mapToDomain(company): CompanyEntity {
        if (company instanceof CompanyOrmEntity) {
            return new CompanyEntity(
                company.id,
                company.shortName,
                company.fullName,
                company.internationalName,
                company.address,
                company.phone,
                company.director,
            );
        }

        return null;
    }

    static mapToOrm(company): CompanyOrmEntity {
        if (!company)
            return null;

        const ormEntity = new CompanyOrmEntity();

        if (company instanceof CompanyEntity) {
            ormEntity.id = company.id;
            ormEntity.shortName = company.shortName;
            ormEntity.fullName = company.fullName;
            ormEntity.internationalName = company.internationalName;
            ormEntity.address = company.address;
            ormEntity.phone = company.phone;
            ormEntity.director = company.director;
        }

        return ormEntity;
    }
}
