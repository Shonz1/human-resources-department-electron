import {Module} from "../../decorators";
import {CompanyAdapter} from "./company.adapter";
import {
    ChangeCompanyService,
    ChangeCompanyUseCaseSymbol,
    CreateCompanyService,
    CreateCompanyUseCaseSymbol,
    FindCompanyQuerySymbol,
    FindCompanyService, RemoveCompanyService, RemoveCompanyUseCaseSymbol
} from "human-resources-department-common";
import {EmployeeAdapter} from "../employee-persistence/employee.adapter";

@Module({
    providers: [
        CompanyAdapter,
        {
            provide: FindCompanyQuerySymbol,
            useFactory: companyAdapter => new FindCompanyService(companyAdapter),
            inject: [CompanyAdapter]
        },
        {
            provide: CreateCompanyUseCaseSymbol,
            useFactory: companyAdapter => new CreateCompanyService(companyAdapter, companyAdapter),
            inject: [CompanyAdapter]
        },
        {
            provide: RemoveCompanyUseCaseSymbol,
            useFactory: (companyAdapter, employeeAdapter) => new RemoveCompanyService(companyAdapter, companyAdapter, employeeAdapter, employeeAdapter),
            inject: [CompanyAdapter, EmployeeAdapter]
        },
        {
            provide: ChangeCompanyUseCaseSymbol,
            useFactory: (companyAdapter, employeeAdapter) => new ChangeCompanyService(companyAdapter, companyAdapter, employeeAdapter),
            inject: [CompanyAdapter, EmployeeAdapter]
        },
    ]
})
export class CompanyPersistenceModule {}
