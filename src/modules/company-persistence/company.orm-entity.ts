import {Column, Entity, PrimaryColumn} from "typeorm";

@Entity('companies')
export class CompanyOrmEntity {
    @PrimaryColumn({name: 'id'})
    id: string;

    @Column({name: 'short_name'})
    shortName: string;

    @Column({name: 'full_name'})
    fullName: string;

    @Column({name: 'international_name'})
    internationalName: string;

    @Column({name: 'address'})
    address: string;

    @Column({name: 'phone'})
    phone: string;

    @Column({name: 'director', nullable: true})
    director?: string;
}
