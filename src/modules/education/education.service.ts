import {Inject, Injectable} from "../../decorators";
import {
    CreateEducationCommand,
    CreateEducationUseCase,
    CreateEducationUseCaseSymbol,
    FindEducationQuery,
    FindEducationQuerySymbol, RemoveEducationCommand, RemoveEducationUseCase, RemoveEducationUseCaseSymbol
} from "human-resources-department-common";
import {EducationMapper} from "./education.mapper";
import {EducationObjectEntity} from "./education.object-entity";

@Injectable()
export class EducationService {
    constructor(
        @Inject(FindEducationQuerySymbol) private readonly _findEducationQuery: FindEducationQuery,
        @Inject(CreateEducationUseCaseSymbol) private readonly _createEducationUseCase: CreateEducationUseCase,
        @Inject(RemoveEducationUseCaseSymbol) private readonly _removeEducationUseCase: RemoveEducationUseCase,
    ) {}

    async findEducationById(educationId: string): Promise<EducationObjectEntity | null> {
        return EducationMapper.mapToObject(await this._findEducationQuery.findEducationById(educationId));
    }

    async findEducationsByPersonId(personId: string): Promise<EducationObjectEntity[]> {
        return (await this._findEducationQuery.findEducationsByPersonId(personId)).map(i => EducationMapper.mapToObject(i));
    }

    async createEducation(personId: string, name: string, level: number): Promise<EducationObjectEntity> {
        return EducationMapper.mapToObject(await this._createEducationUseCase.createEducation(
            new CreateEducationCommand(
                personId,
                name,
                level,
            )
        ));
    }

    async removeEducation(educationId: string): Promise<void> {
        return await this._removeEducationUseCase.removeEducation(
            new RemoveEducationCommand(educationId)
        );
    }
}
