import {Controller, Inject, Route} from "../../decorators";
import {EducationService} from "./education.service";

@Controller('education')
export class EducationController {
    constructor(
        @Inject(EducationService) private readonly _educationService: EducationService,
    ) {}

    @Route('findEducationById')
    async findEducationById(educationId: string) {
        return await this._educationService.findEducationById(educationId);
    }

    @Route('findEducationsByPersonId')
    async findEducationsByPersonId(personId: string) {
        return await this._educationService.findEducationsByPersonId(personId);
    }

    @Route('createEducation')
    async createEducation(personId: string, name: string, level: number) {
        return await this._educationService.createEducation(personId, name, level);
    }

    @Route('removeEducation')
    async removeEducation(educationId: string) {
        return await this._educationService.removeEducation(educationId);
    }
}
