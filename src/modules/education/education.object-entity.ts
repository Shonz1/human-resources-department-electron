export class EducationObjectEntity {
    id: string;
    personId: string;
    name: string;
    level: number;
}
