import {Module} from "../../decorators";
import {EducationService} from "./education.service";
import {EducationController} from "./education.controller";

@Module({
    providers: [
        EducationService,
        EducationController,
    ]
})
export class EducationModule {}
