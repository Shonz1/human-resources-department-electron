import {EducationEntity} from "human-resources-department-common";
import {EducationOrmEntity} from "../education-persistence/education.orm-entity";
import {EducationObjectEntity} from "./education.object-entity";

export class EducationMapper {
    static mapToDomain(education): EducationEntity {
        if (education instanceof EducationOrmEntity) {
            return new EducationEntity(
                education.id,
                education.personId,
                education.name,
                education.level,
            );
        }

        return null;
    }

    static mapToObject(education): EducationObjectEntity {
        if (!education)
            return null;

        const obj = new EducationObjectEntity();

        if (education instanceof EducationEntity) {
            obj.id = education.id;
            obj.personId = education.personId;
            obj.name = education.name;
            obj.level = education.level;
        }

        return obj;
    }
}
