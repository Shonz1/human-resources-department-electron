export class PassportObjectEntity {
    id: string;
    personId: string;
    serial: string;
    number: string;
    issuer: string;
    issuedAt: Date;
}
