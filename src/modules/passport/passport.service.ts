import {
    CreatePassportCommand,
    CreatePassportUseCase,
    CreatePassportUseCaseSymbol,
    FindPassportQuery,
    FindPassportQuerySymbol,
    RemovePassportCommand, RemovePassportUseCase, RemovePassportUseCaseSymbol
} from "human-resources-department-common";
import {PassportMapper} from "./passport.mapper";
import {Inject, Injectable} from "../../decorators";

@Injectable()
export class PassportService {
    constructor(
        @Inject(FindPassportQuerySymbol) private readonly _findPassportQuery: FindPassportQuery,
        @Inject(CreatePassportUseCaseSymbol) private readonly _createPassportUseCase: CreatePassportUseCase,
        @Inject(RemovePassportUseCaseSymbol) private readonly _removePassportUseCase: RemovePassportUseCase,
    ) {}

    async findPassportById(passportId: string) {
        return PassportMapper.mapToObject(await this._findPassportQuery.findPassportById(passportId));
    }

    async findPassportBySerialAndNumber(serial: string, number: string) {
        return PassportMapper.mapToObject(await this._findPassportQuery.findPassportBySerialAndNumber(serial, number));
    }

    async findPassportsByPersonId(personId: string) {
        return (await this._findPassportQuery.findPassportsByPersonId(personId)).map(i => PassportMapper.mapToObject(i));
    }

    async findAllPassports() {
        return (await this._findPassportQuery.findAllPassports()).map(i => PassportMapper.mapToObject(i));
    }

    async createPassport(personId: string, serial: string, number: string, issuer: string, issuedAt: Date) {
        const command = new CreatePassportCommand(
            personId,
            serial,
            number,
            issuer,
            issuedAt,
        );

        return PassportMapper.mapToObject(await this._createPassportUseCase.createPassport(
            command
        ));
    }

    async removePassport(passportId: string) {
        await this._removePassportUseCase.removePassport(
            new RemovePassportCommand(passportId)
        );
    }
}
