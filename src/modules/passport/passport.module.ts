import {Module} from "../../decorators";
import {PassportService} from "./passport.service";
import {PassportController} from "./passport.controller";

@Module({
    providers: [
        PassportService,
        PassportController,
    ]
})
export class PassportModule {}
