import {PassportEntity} from "human-resources-department-common";
import {PassportObjectEntity} from "./passport.object-entity";

export class PassportMapper {
    static mapToDomain(passport): PassportEntity {
        if (passport instanceof PassportObjectEntity) {
            return new PassportEntity(
                passport.id,
                passport.personId,
                passport.serial,
                passport.number,
                passport.issuer,
                passport.issuedAt,
            );
        }

        return null;
    }

    static mapToObject(passport): PassportObjectEntity {
        if (!passport)
            return null;

        const obj = new PassportObjectEntity();

        if (passport instanceof PassportEntity) {
            obj.id = passport.id;
            obj.personId = passport.personId;
            obj.serial = passport.serial;
            obj.number = passport.number;
            obj.issuer = passport.issuer;
            obj.issuedAt = passport.issuedAt;
        }

        return obj;
    }
}
