import {Controller, Inject, Route} from "../../decorators";
import {PassportService} from "./passport.service";

@Controller('passport')
export class PassportController {
    constructor(
        @Inject(PassportService) private readonly _passportService: PassportService,
    ) {}

    @Route('findPassportById')
    async findPassportById(passportId: string) {
        return await this._passportService.findPassportById(passportId);
    }

    @Route('findPassportBySerialAndNumber')
    async findPassportBySerialAndNumber(serial: string, number: string) {
        return await this._passportService.findPassportBySerialAndNumber(serial, number);
    }

    @Route('findPassportsByPersonId')
    async findPassportsByPersonId(personId: string) {
        return await this._passportService.findPassportsByPersonId(personId);
    }

    @Route('findAllPassports')
    async findAllPassports() {
        return await this._passportService.findAllPassports();
    }

    @Route('createPassport')
    async createPassport(personId: string, serial: string, number: string, issuer: string, issuedAt: Date) {
        return await this._passportService.createPassport(personId, serial, number, issuer, issuedAt);
    }

    @Route('removePassport')
    async removePassport(passportId: string) {
        return await this._passportService.removePassport(passportId);
    }
}
