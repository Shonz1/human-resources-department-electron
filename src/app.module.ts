import {Module} from "./decorators";
import {EducationPersistenceModule} from "./modules/education-persistence/education-persistence.module";
import {PassportPersistenceModule} from "./modules/passport-persistence/passport-persistence.module";
import {PersonPersistenceModule} from "./modules/person-persistence/person-persistence.module";
import {EmployeePersistenceModule} from "./modules/employee-persistence/employee-persistence.module";
import {CompanyPersistenceModule} from "./modules/company-persistence/company-persistence.module";
import {EducationModule} from "./modules/education/education.module";
import {PassportModule} from "./modules/passport/passport.module";
import {PersonModule} from "./modules/person/person.module";
import {EmployeeModule} from "./modules/employee/employee.module";
import {CompanyModule} from "./modules/company/company.module";

@Module({
    providers: [
        EducationPersistenceModule,
        PassportPersistenceModule,
        PersonPersistenceModule,
        EmployeePersistenceModule,
        CompanyPersistenceModule,

        EducationModule,
        PassportModule,
        PersonModule,
        EmployeeModule,
        CompanyModule,
    ]
})
export class AppModule {}
