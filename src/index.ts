import "reflect-metadata";
import * as path from 'path';
import {app, BrowserWindow} from "electron";
import {createDbConnection} from "./db";

async function bootstrap() {
    await createDbConnection(`${app.getAppPath()}/data/data.sqlite`);

    await import('./app.module');

    await app.whenReady();

    app.on('window-all-closed', () => process.platform !== 'darwin' && app.quit());

    const window = new BrowserWindow({
        width: 800,
        minWidth: 800,
        height: 500,
        minHeight: 500,
        autoHideMenuBar: true,
        darkTheme: true,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            devTools: true,
        }
    });

    if (app.isPackaged) {
        await window.loadFile(path.join(__dirname, '..', 'public', 'index.html'));
    } else {
        await window.loadURL('http://localhost:3000');
    }
}
bootstrap();
